import logoLetu from '../assets/logos/LETUShield/LETUShield-FllClrBlue.svg'
import logoACM from '../assets/logos/ACM-Standalone/ACM.svg'

export default function Letufooter() {
    return (
        <footer className="absolute bottom-0 left-0 w-full">
            <div className="bg-grey w-full p-3 flex">
                <img src={logoLetu} className="h-24" />
                <img src={logoACM} className="h-24" />
            </div>
        </footer>
    )
}
